﻿using System;
using System.Threading;

namespace MultithreadingLocks
{
    public class Balance
    {
        private const long MaxAllowedAmount = 100_000;
        private const long MaxAmountPerTransaction = 10_000;

        public long Amount { get; private set; } = 0;

        public Balance(long initialAmount)
        {
            if (initialAmount < 0)
            {
                throw new ArgumentException("The amount should be equal or greater than 0");
            }

            if (initialAmount > MaxAllowedAmount)
            {
                throw new ArgumentException("The amount should be greater than 0");
            }

            Amount = initialAmount;
        }

        public void Add(int amountToAdd)
        {
            if (amountToAdd <= 0)
            {
                throw new ArgumentException("The amount should be greater than 0.");
            }

            if (amountToAdd > MaxAmountPerTransaction)
            {
                throw new ArgumentException($"The value {amountToAdd} exceeds transaction limit: {MaxAmountPerTransaction}.");
            }

            if (Amount + amountToAdd > MaxAllowedAmount)
            {
                throw new ArgumentException("Cannot add the specified amount: the sum exceeds account limit.");
            }

            AddAmountAndEmulateTransactionDelay(amountToAdd);
        }

        public void Withdraw(int amountToWithdraw)
        {
            if (amountToWithdraw <= 0)
            {
                throw new ArgumentException("The amount should be greater than 0.");
            }

            if (amountToWithdraw > MaxAmountPerTransaction)
            {
                throw new ArgumentException($"The value {amountToWithdraw} exceeds transaction limit: {MaxAmountPerTransaction}.");
            }

            if (amountToWithdraw > Amount)
            {
                throw new ArgumentException("Insufficient funds.");
            }

            WithdrawAndEmulateTransactionDelay(amountToWithdraw);
        }

        #region private methods

        /// <summary>
        /// This method is needed to reproduce conflicts during parallel execution.
        /// Please do not change it.
        /// </summary>
        private void AddAmountAndEmulateTransactionDelay(int amountToAdd)
        {
            Thread.Sleep(1000);
            Amount += amountToAdd;
        }

        /// <summary>
        /// This method is needed to reproduce conflicts during parallel execution.
        /// Please do not change it.
        /// </summary>
        private void WithdrawAndEmulateTransactionDelay(int amountToWithdraw)
        {
            Thread.Sleep(1000);
            Amount -= amountToWithdraw;
        }

        #endregion
    }
}
