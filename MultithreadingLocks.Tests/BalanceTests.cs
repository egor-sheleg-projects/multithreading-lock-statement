﻿using System;
using System.Threading.Tasks;
using NUnit.Framework;

namespace MultithreadingLocks.Tests
{
    /// <summary>
    /// Tests for balance calculation.
    /// </summary>
    [TestFixture]
    public class BalanceTests
    {
        private const long InitialAmont = 12000;

        /// <summary>
        /// Checks if the Amout initializes correctly in constructor of Balance class.
        /// </summary>
        [Test]
        public void BalanceInitTest()
        {
            var balance = new Balance(InitialAmont);

            Assert.AreEqual(InitialAmont, balance.Amount);
        }

        /// <summary>
        /// Tests if parallel withdrawal of two amounts throws argument exception if the sum of
        /// them is more than the total amount.
        /// </summary>
        [Test]
        public void ParallelWithdrawalTest()
        {
            var balance = new Balance(InitialAmont);

            var withdrawTenThTask = Task.Run(() => balance.Withdraw(10000));
            var withdrawFiveThTask = Task.Run(() => balance.Withdraw(5000));

            Assert.Throws(
                typeof(AggregateException),
                () => Task.WaitAll(withdrawFiveThTask, withdrawTenThTask),
                "Insufficient funds.");

            if (withdrawFiveThTask.Exception != null)
            {
                // it means withdrawFiveThTask failed but 10 000 should be withdrawn successfully.
                Assert.IsInstanceOf<AggregateException>(withdrawFiveThTask.Exception);
                Assert.IsInstanceOf<ArgumentException>(withdrawFiveThTask.Exception.InnerException);
                Assert.AreEqual(2000, balance.Amount);
            }
            else
            {
                // it means withdrawTenThTask failed but 5 000 should be withdrawn successfully.
                Assert.IsInstanceOf<AggregateException>(withdrawTenThTask.Exception);
                Assert.IsInstanceOf<ArgumentException>(withdrawTenThTask.Exception.InnerException);
                Assert.AreEqual(7000, balance.Amount);
            }
        }

        /// <summary>
        /// Tests if parallel adding of two amounts throws an exception if the sum exceeds maximum of the allowed amount.
        /// </summary>
        [Test]
        public void ParallelAddAmountTest()
        {
            var balance = new Balance(90000);

            var withdrawTenThTask = Task.Run(() => balance.Add(10000));
            var withdrawFiveThTask = Task.Run(() => balance.Add(10000));

            Assert.Throws(
                typeof(AggregateException),
                () => Task.WaitAll(withdrawFiveThTask, withdrawTenThTask),
                "Cannot add the specified amount: the sum exceeds account limit.");            

            Assert.AreEqual(100000, balance.Amount);
        }
    }
}
