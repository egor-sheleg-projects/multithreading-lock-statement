# multithreading-lock-statement

The task will allow you to practice the implementation of the lock statement and control access to shared resources. 

**Task Description** 

1. Download the project from [git](https://gitlab.com/epam-autocode-tasks/multithreading-lock-statement). 

1. Using the _lock construction_, update the _Balance class_ to execute the following methods in parallel:
   - void Withdraw(int amountToWithdraw)
   - void Add(int amountToAdd)

   Note: **do not change the following methods and do not remove the code that calls them**: 
   
   - WithdrawAndEmulateTransactionDelay 
   - AddAmountAndEmulateTransactionDelay 

1. Run all unit tests in the project MultithreadingLocks.Tests with Visual Studion and make sure there are no failed unit tests. Fix your code to make all tests GREEN. 

1. Upload the results to AutoCode. 

Make sure to review the article on the [lock statement](https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/keywords/lock-statement) before you start working on the project.    
